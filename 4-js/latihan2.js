const solution = (n) => {
  if (n >= 91 && n <= 100) {
    return "A";
  } else if (n >= 81 && n <= 90) {
    return "B";
  } else if (n >= 71 && n <= 80) {
    return "C";
  } else if (n >= 61 && n <= 70) {
    return "D";
  } else if (n <= 60) {
    return "E";
  }
};

console.log(solution(95));
console.log(solution(76));
