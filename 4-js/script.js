const luasPersegi = (sisi) => {
  return sisi * sisi;
};

const luasSegitiga = (alas, tinggi) => {
  return (alas * tinggi) / 2;
};

console.log(luasPersegi(3));
console.log(luasSegitiga(4, 4));
console.log(luasSegitiga(4, 10));
