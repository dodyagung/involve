const solution = (x, y) => {
  if (x % 5 !== 0) {
    return y;
  }

  const result = y - x - 0.5;

  if (x > y || result < 0) {
    return "saldo kurang";
  }

  return result;
};

console.log(solution(30, 20));
console.log(solution(42, 100));
console.log(solution(40, 40));
console.log(solution(50, 100));
